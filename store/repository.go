package store

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"time"
)

//Repository ...
type Repository struct{}

// SERVER the DB server
const SERVER = "mongodb://127.0.0.1:27017/testStore"

// DBNAME the name of the DB instance
const DBNAME = "testStore"

// COLLECTION is the name of the collection in DB
const COLLECTION = "testStore"

// Number of users per page

const PAGE = 5

// GetProducts returns the list of Products
func (r Repository) GetUsers() Users {
	session, err := mgo.Dial(SERVER)

	if err != nil {
		fmt.Println("Failed to establish connection to Mongo server:", err)
	}

	defer session.Close()

	c := session.DB(DBNAME).C(COLLECTION)
	results := Users{}

	if err := c.Find(nil).Limit(PAGE).All(&results); err != nil {
		fmt.Println("Failed to write results:", err)
	}

	return results
}

func (r Repository) SearchUserByName(login string, password string) (User, error) {
	session, err := mgo.Dial(SERVER)
	if err != nil {
		fmt.Println("Failed to establish connection to Mongo server:", err)
	}
	defer session.Close()

	c := session.DB(DBNAME).C(COLLECTION)
	var result User
	and := make([]bson.M, 2)
	and[0] = bson.M{"username": login}
	and[1] = bson.M{"password": getMd5Hash(password)}
	filter := bson.M{"$and": and}
	if err := c.Find(filter).One(&result); err != nil {
		fmt.Println("There is no user")
		return result, errors.New("There is no User " + err.Error())
	}
	result.LastEnter = time.Now().UnixNano() / 1000000
	err = c.UpdateId(result.ID, result)
	if err != nil {
		fmt.Println("Can't update user info ")
		return result, errors.New("Can't update user info " + err.Error())
	}
	return result, nil
}

// GetProductById returns a unique Product
func (r Repository) GetUserById(id int) User {
	session, err := mgo.Dial(SERVER)

	if err != nil {
		fmt.Println("Failed to establish connection to Mongo server:", err)
	}

	defer session.Close()

	c := session.DB(DBNAME).C(COLLECTION)
	var result User

	fmt.Println("ID in GetProductById", id)

	if err := c.FindId(id).One(&result); err != nil {
		fmt.Println("Failed to write result:", err)
	}

	return result
}

// GetProductsByString takes a search string as input and returns products
func (r Repository) GetUserByString(query UserQuery) UserResponce {
	session, err := mgo.Dial(SERVER)
	if err != nil {
		fmt.Println("Failed to establish connection to Mongo server:", err)
	}

	defer session.Close()

	c := session.DB(DBNAME).C(COLLECTION)
	result := UserResponce{}
	and := []bson.M{}
	if len(query.AgeCodes) != 0 {
		and = append(and, bson.M{"agecode": bson.M{"$in": query.AgeCodes}})
	}
	if len(query.Interest) != 0 {
		for _, i := range query.Interest {
			and = append(and, bson.M{"interest." + i: 1})
		}
	}
	if len(query.NotInterest) != 0 {
		for _, i := range query.Interest {
			and = append(and, bson.M{"$not": bson.M{"interest." + i: 1}})
		}
	}
	and = append(and, bson.M{"_id": bson.M{"$ne": query.ID}})

	filter := bson.M{"$and": and}
	log.Println("Get the users")
	users := Users{}
	if err := c.Find(&filter).Limit(PAGE).Skip(query.Offset).All(&users); err != nil {
		log.Println("Failed to write result:", err.Error())
	}
	result.Users = users
	result.NextOffset = query.Offset + PAGE
	return result
}

// AddProduct adds a Product in the DB
func (r Repository) AddUser(info User) bool {
	session, err := mgo.Dial(SERVER)
	defer session.Close()

	c := session.DB(DBNAME).C(COLLECTION)

	num, err := c.Find(bson.M{"username": info.Username}).Count()

	if err != nil || num > 0 {
		fmt.Println("User already exists " + info.Username)
		return false
	}
	var res User
	info.Password = getMd5Hash(info.Password)
	err = c.Find(bson.M{}).Sort("-_id").Limit(1).One(&res)
	fmt.Println(res)
	if err != nil {
		fmt.Println("New user create error " + err.Error())
	}
	info.ID = res.ID + 1
	info.LastEnter = time.Now().UnixNano() / 1000000
	c.Insert(info)
	if err != nil {
		log.Fatal(err)
		return false
	}

	fmt.Println("Added New user ID- ", info.ID)

	return true
}

// UpdateProduct updates a Product in the DB
func (r Repository) UpdateUser(info User) bool {
	session, err := mgo.Dial(SERVER)
	defer session.Close()

	err = session.DB(DBNAME).C(COLLECTION).UpdateId(info.ID, info)

	if err != nil {
		log.Fatal(err)
		return false
	}

	fmt.Println("Updated Product ID - ", info.ID)

	return true
}

// DeleteProduct deletes an Product
func (r Repository) DeleteUser(id int) error {
	session, err := mgo.Dial(SERVER)
	defer session.Close()

	// Remove product
	if err = session.DB(DBNAME).C(COLLECTION).RemoveId(id); err != nil {
		log.Fatal(err)
		return err
	}

	fmt.Println("Deleted Product ID - ", id)
	// Write status
	return nil
}

func getMd5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}
