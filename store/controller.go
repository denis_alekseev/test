package store

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
)

//Controller ...
type Controller struct {
	Repository Repository
}

/* Middleware handler to handle all requests for authentication */
func AuthenticationMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		authorizationHeader := req.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("There was an error")
					}
					return []byte("secret"), nil
				})
				if error != nil {
					json.NewEncoder(w).Encode(Exception{Message: error.Error()})
					return
				}
				if token.Valid {
					log.Println("TOKEN WAS VALID")
					context.Set(req, "decoded", token.Claims)
					next(w, req)
				} else {
					json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
				}
			}
		} else {
			json.NewEncoder(w).Encode(Exception{Message: "An authorization header is required"})
		}
	})
}

// Get Authentication token POST /get-token
func (c *Controller) GetToken(w http.ResponseWriter, req *http.Request) {
	var user User
	_ = json.NewDecoder(req.Body).Decode(&user)
	user, err := c.Repository.SearchUserByName(user.Username, user.Password)
	if err != nil {
		msg := make(map[string]string)
		msg["success"] = "false"
		msg["message"] = err.Error()
		json.NewEncoder(w).Encode(msg)
		return
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user.Username,
		"password": user.Password,
		"id":       user.ID,
	})

	tokenString, error := token.SignedString([]byte("secret"))
	if error != nil {
		fmt.Println(error)
	}
	msg := map[string]string{"success": "true", "token": tokenString}
	json.NewEncoder(w).Encode(msg)
}

// Index GET /
func (c *Controller) Index(w http.ResponseWriter, r *http.Request) {
	users := c.Repository.GetUsers() // list of all products
	// log.Println(products)
	data, _ := json.Marshal(users)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
	return
}

// Register User POST /register-user
func (c *Controller) RegisterUser(w http.ResponseWriter, r *http.Request) {
	var userInfo User
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request

	if err != nil {
		log.Fatalln("Error register user", err)
		w.WriteHeader(http.StatusInternalServerError)
		msg := map[string]string{"success": "False", "message": "User not registered"}
		json.NewEncoder(w).Encode(msg)
		return
	}

	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error register user", err)
	}

	if err := json.Unmarshal(body, &userInfo); err != nil { // unmarshall body contents as a type Candidate
		w.WriteHeader(422) // unprocessable entity
		log.Println(err)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error register user unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			msg := map[string]string{"success": "False", "message": "User not registered"}
			json.NewEncoder(w).Encode(msg)
			return
		}
	}

	success := c.Repository.AddUser(userInfo) // adds the product to the DB
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		msg := map[string]string{"success": "False", "message": "User not registered"}
		json.NewEncoder(w).Encode(msg)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	msg := map[string]string{"success": "True", "message": "User was registered"}
	json.NewEncoder(w).Encode(msg)
	return
}

func (c *Controller) SearchUser(w http.ResponseWriter, r *http.Request) {
	vars := r.URL.Query()
	log.Println(vars)

	param, _ := vars["offset"] // param id

	offset, err := strconv.Atoi(param[0])

	if err != nil {
		offset = 0
	}

	query := UserQuery{}
	users := UserResponce{}
	authorizationHeader := r.Header.Get("authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There was an error")
				}
				return []byte("secret"), nil
			})
			if error != nil {
				json.NewEncoder(w).Encode(Exception{Message: error.Error()})
				return
			}
			if token.Valid {
				user := token.Claims.(jwt.MapClaims)
				userid := int(user["id"].(float64))
				query.ID = userid
				query = c.makeUserQuery(userid)
			}
		}
	}
	query.Offset = offset
	users = c.Repository.GetUserByString(query)
	data, _ := json.Marshal(users)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
	return
}

// UpdateUser PUT /
func (c *Controller) UpdateUser(w http.ResponseWriter, r *http.Request) {
	var info User
	var doNotTuch = []string{"ID", "Username", "Password", "LastEnter", "LastGeo"}
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request
	if err != nil {
		log.Fatalln("Error UpdateProduct", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error UpdateProduct", err)
	}

	if err := json.Unmarshal(body, &info); err != nil { // unmarshall body contents as a type Candidate
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		log.Println(err.Error())
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error Update User unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	user := context.Get(r, "decoded").(jwt.MapClaims)
	userid := int(user["id"].(float64))
	newUser := c.Repository.GetUserById(userid)

	s1 := reflect.ValueOf(&info).Elem()
	s2 := reflect.ValueOf(&newUser).Elem()
	t := s1.Type()

	for i := 0; i < s1.NumField(); i++ {
		e1 := s1.Field(i)
		e2 := s2.Field(i)
		nm := t.Field(i).Name
		in, _ := InArray(nm, doNotTuch)
		if !IsEmptyValue(e1) && !in {
			e2.Set(reflect.Value(e1))
		}
	}

	//info.ID = userid
	newUser.AgeCode = getAgeCode(newUser.Age)
	success := c.Repository.UpdateUser(newUser) // updates the product in the DB
	resp := UserResponce{}
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Status = "False"
		return
	}
	resp.Status = "True"
	resp.NextOffset = 0
	resp.Users = append(resp.Users, newUser)
	data, _ := json.Marshal(resp)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
	return
}

// GetProduct GET - Gets a single product by ID /
func (c *Controller) GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"] // param id
	userid, err := strconv.Atoi(id)
	resp := UserResponce{}
	if err != nil {
		log.Fatalln("Error GetProduct", err)
	}

	user := c.Repository.GetUserById(userid)
	resp.Status = "True"
	resp.NextOffset = 0
	resp.Users = append(resp.Users, user)
	data, _ := json.Marshal(resp)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
	return
}

// DeleteUser DELETE /delete-my
func (c *Controller) DeleteUser(w http.ResponseWriter, r *http.Request) {
	resp := make(map[string]string)
	user := context.Get(r, "decoded").(jwt.MapClaims)
	userid := int(user["id"].(float64))
	if err := c.Repository.DeleteUser(userid); err != nil { // delete a user by id
		if strings.Contains(err.Error(), "404") {
			w.WriteHeader(http.StatusNotFound)
			resp["success"] = "false"
		} else if strings.Contains(err.Error(), "500") {
			w.WriteHeader(http.StatusInternalServerError)
			resp["success"] = "false"
		}
		data, _ := json.Marshal(resp)
		w.Write(data)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	resp["success"] = "true"
	data, _ := json.Marshal(resp)
	w.Write(data)
	return
}

func getAgeCode(age int) int {
	// В продакшене так делать не надо все диапазоны дожны настраиваться хотя бы на уровне конфигов, а лучше админки
	// Но в тестовом задании сделаю так что бы не городить огород
	code := 0
	if age > 18 && age <= 24 {
		return 1
	} else if age > 25 && age <= 40 {
		return 2
	} else if age > 40 {
		return 3
	}
	return code
}

func (c Controller) makeUserQuery(id int) UserQuery {
	query := UserQuery{}
	if id != 0 {
		user := c.Repository.GetUserById(id)
		query.ID = id
		query.AgeCodes = user.ShowAges
		query.Interest = user.ShowIntersts
		query.NotInterest = user.DoNotShowIntersts
	} else {
		query.AgeCodes = []int8{1, 2, 3}
		query.Interest = []string{"interest1", "interest2", "interest3"}
	}
	return query
}

func IsEmptyValue(e reflect.Value) bool {
	is_empty := true
	k := e.Type().Kind()
	switch k {
	case reflect.String:
		if e.String() != "" {
			is_empty = false
		}
	case reflect.Array:
		for j := e.Len() - 1; j >= 0; j-- {
			is_empty = IsEmptyValue(e.Index(j))
			if is_empty == false {
				break
			}
		}
	case reflect.Map, reflect.Slice:
		if e.Len() != 0 {
			is_empty = false
		}

	case reflect.Float32, reflect.Float64:
		if e.Float() != 0 {
			is_empty = false
		}
	case reflect.Int32, reflect.Int64, reflect.Int:
		if e.Int() != 0 {
			is_empty = false

		}
	}
	return is_empty
}

func InArray(needle interface{}, haystack interface{}) (exists bool, index int) {
	exists = false
	index = -1

	switch reflect.TypeOf(haystack).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(haystack)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(needle, s.Index(i).Interface()) == true {
				index = i
				exists = true
				return
			}
		}
	}

	return
}
