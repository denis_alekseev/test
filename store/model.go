package store

type JwtToken struct {
	Token string `json:"token"`
}

type Exception struct {
	Message string `json:"message"`
}

// User represents an user item
type User struct {
	ID                int            `bson:"_id"`
	Username          string         `json:"username"`
	Password          string         `json:"password"`
	Name              string         `json:"name"`
	Avatar            string         `json:"avatar"`
	Age               int            `json:"age"`
	AgeCode           int            `json:"ageCode"`
	Interest          map[string]int `json:"interest"`
	ShowAges          []int8         `json:"showAges"`
	NotShowAges       []int8         `json:"notShowAges"`
	ShowIntersts      []string       `json:"showInterest"`
	DoNotShowIntersts []string       `json:"doNotShowIntersts"`
	LastEnter         int64          `json:"lastEnter"`
	LastGeo           string         `json:"lastGeo"`
}

// Users is an array of User objects
type Users []User

type UserQuery struct {
	ID          int      `json:"id"`
	AgeCodes    []int8   `json:"ageCodes"`
	Interest    []string `json:"interestCodes"`
	NotInterest []string `json:"notInterestCodes"`
	Offset      int      `json:"offset"`
}
type UserResponce struct {
	Status     string `json:"status"`
	Users      Users  `json:"users"`
	NextOffset int    `json:"nextOffset"`
}
