##Требования

Go 1.11  
vgo
```
go get -u golang.org/x/vgo
```
mongodb  

##Запуск

```
vgo build
```

##Протокол работы

###Регистрация

**url** http:\\localhost:8080\register-user  
**method** POST  
**body**
```json
{
  "username":"aaa",
  "password":"bbb"
}
```
**ответ**
```json
{
  "success": "True or False",
  "message": "..."
}
```

###Авторизация
**url** http:\\localhost:8080\register-user  
**method** POST  
**body** 
```json
{
  "username":"aaa",
  "password":"bbb"
 }
```
**ответ**
```json
{
  "success":"true",
  "token": "[токен для изменения и удаления пользователя]"
}
```
###Редактирование данных пользователя
**url** http:\\localhost:8080\update-user  
**method** PUT  
**Headers**  
```
Authorization: [smb string] [token]
```
**body**
```json
{
	"name": string,
	"avatar": string,
	"age": int,
	"interest": {"interest1":1,"interest2":-1},
	"showAges": [1,2,3],
	"notShowAges": [1,2,3],
	"showInterest": ["interest1","interest2"]
}
``` 
Все поля опционыльны и могут отсутствовать. Если полк отсутствует то его значение в БД остаётся без изменений  
Аватарки не загружаются если понадобиться сделать это не сложно
###Поиск пользователей
**url** http:\\localhost:8080\search  
**method** GET
**Headers**  
```
Authorization: [smb string] [token]
```
Заголовок не обязателен. Если заголовка нет ищутся все пользователи в порядке добавления. 
Если токен передаётся в ответ приходит список пользователей в соответствии с настройками поиска.   
В данной реализации не делал поиска и сортировки по георафическому положению пользователя.
Задача не особо сложная, но лучше её делать с использованием ElasticSearch или подобных инструментов 
которые позволяют создавать нормальные гео-индексы. Если не использовать индексацию по гео-полям
то поиск может начать сильно "тормозить" даже на небольших выборках   

###Для запуска тестов 
```
    vgo test
```