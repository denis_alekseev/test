package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	. "github.com/smartystreets/goconvey/convey"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"test/store"
	"testing"
)

var token string

func TestRegister(t *testing.T) {
	Convey("Register new user test", t, func() {
		url := "http://127.0.0.1:8080/register-user"
		values := map[string]string{"username": "kva10", "password": "bbb"}
		jsonValue, _ := json.Marshal(values)
		resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
		So(err, ShouldEqual, nil)
		So(resp.StatusCode, ShouldEqual, 201)
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		So(err, ShouldEqual, nil)
		var res map[string]string
		err = json.Unmarshal(body, &res)
		So(err, ShouldEqual, nil)
		So(res["success"], ShouldEqual, "True")
	})
}

func TestGetToken(t *testing.T) {
	Convey("Get user token test", t, func() {
		url := "http://127.0.0.1:8080/get-token"
		values := map[string]string{"username": "kva10", "password": "bbb"}
		jsonValue, _ := json.Marshal(values)
		resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
		So(err, ShouldEqual, nil)
		So(resp.StatusCode, ShouldEqual, 200)
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		var res map[string]string
		err = json.Unmarshal(body, &res)
		So(err, ShouldEqual, nil)
		So(res["success"], ShouldEqual, "true")
		token = res["token"]
	})
}

func TestGetUser(t *testing.T) {
	userInfo, error := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return []byte("secret"), nil
	})
	if error != nil {
		fmt.Println(error.Error())
	}
	claims := userInfo.Claims.(jwt.MapClaims)
	userId := int(claims["id"].(float64))
	Convey("Test get user", t, func() {
		url := "http://127.0.0.1:8080/user/" + strconv.Itoa(userId)
		resp, err := http.Get(url)
		So(err, ShouldEqual, nil)
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		So(err, ShouldEqual, nil)
		var res store.UserResponce
		err = json.Unmarshal(body, &res)
		So(err, ShouldEqual, nil)
		So(res.Status, ShouldEqual, "True")
	})
}

func TestUpdateUser(t *testing.T) {
	userInfo, error := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return []byte("secret"), nil
	})
	if error != nil {
		fmt.Println(error.Error())
	}
	claims := userInfo.Claims.(jwt.MapClaims)
	userId := int(claims["id"].(float64))
	Convey("Test update user", t, func() {
		fmt.Println(userId)
		url := "http://127.0.0.1:8080/update-user"
		client := http.Client{}
		request := `{"Age":60}`
		req, err := http.NewRequest("PUT", url, strings.NewReader(request))
		So(err, ShouldEqual, nil)
		req.Header.Set("Authorization", "kva "+token)
		resp, err := client.Do(req)
		So(err, ShouldEqual, nil)
		body, err := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		fmt.Println(string(body))
		var res store.UserResponce
		err = json.Unmarshal(body, &res)
		So(err, ShouldEqual, nil)
		So(res.Status, ShouldEqual, "True")
		So(res.Users[0].AgeCode, ShouldEqual, 3)
	})
}

func TestDeleteUser(t *testing.T) {
	Convey("Test delete user", t, func() {
		client := &http.Client{}
		url := "http://127.0.0.1:8080/delete-my"
		req, err := http.NewRequest("DELETE", url, nil)
		So(err, ShouldEqual, nil)
		req.Header.Set("Authorization", "kva "+token)
		resp, err := client.Do(req)
		So(err, ShouldEqual, nil)
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		var res map[string]string
		err = json.Unmarshal(body, &res)
		So(err, ShouldEqual, nil)
		So(res["success"], ShouldEqual, "true")
	})
}
